@extends('mainlayout')

@section('main')

    <!-- ======= About Section ======= -->
    <!-- ======= About Section ======= -->
    <section id="about" class="about">
        <div class="container">
  
          <div class="section-title">
            <h2>About Me</h2>
          </div>
  
          <div class="row">
            <div class="col-lg-4" data-aos="fade-right">
                <img src="img/profile-img.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-lg-8 pt-4 pt-lg-0 content" data-aos="fade-left">
              <h3>Rahmat Gah Bahaduri</h3>
              <div class="row">
                <div class="col-lg-6">
                  <ul>
                    <li><i class="bi bi-chevron-right"></i> <strong>Birthday:</strong> <span>31 July 2000</span></li>
                    <li><i class="bi bi-chevron-right"></i> <strong>Website:</strong> <span>www.RGB.com</span></li>
                    <li><i class="bi bi-chevron-right"></i> <strong>Phone:</strong> <span>+62 896 1312 7209</span></li>
                    <li><i class="bi bi-chevron-right"></i> <strong>City:</strong> <span>Singaraja, Indonesia</span></li>
                  </ul>
                </div>
                <div class="col-lg-6">
                  <ul>
                    <li><i class="bi bi-chevron-right"></i> <strong>Age:</strong> <span>21</span></li>
                    <li><i class="bi bi-chevron-right"></i> <strong>Degree:</strong> <span>Collage Student</span></li>
                    <li><i class="bi bi-chevron-right"></i> <strong>Email:</strong> <span>rahmat@undiksha.ac.id</span></li>
                    <li><i class="bi bi-chevron-right"></i> <strong>Job:</strong> <span>Searching</span></li>
                  </ul>
                </div>
              </div>
              <p>
                Life must go on, even though problems and trials keep coming. Giving up is not an option, if we are struggling to achieve something.
                Never give up before all the expected goals can be achieved. Failure is a natural thing, the most important thing is to keep trying and never stop in the middle of the road. 
                All the ordeals that we go through will end with something sweet, if we go through it with sincerity, not complaining, and full of enthusiasm.
                Not All Endeavors Require Hard Work Struggle is What Needed To Survive. Your Own Choice Will Determine Your Steps. Whats Your Choice? -RahmatGahBahaduri
            </p>
              </p>
            </div>
          </div>
  
        </div>
      </section><!-- End About Section -->

    <!-- ======= Skills Section ======= -->
    <section id="skills" class="skills section-bg" >
        <div class="container" >

        <div class="section-title" data-aos="fade-up">
            <h2>Skills</h2>

        <div class="row skills-content">

            <div class="col-lg-6" data-aos="fade-up">

            <div class="progress">
                <span class="skill">Multimedia <i class="val">80%</i></span>
                <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>

            <div class="progress">
                <span class="skill">Programming <i class="val">80%</i></span>
                <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>

            <div class="progress">
                <span class="skill">Simple Task <i class="val">95%</i></span>
                <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>

            <div class="progress">
                <span class="skill">Romantic <i class="val">100%</i></span>
                <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>

            </div>

            <div class="col-lg-6"data-aos="fade-up">

            <div class="progress">
                <span class="skill">Assistance <i class="val">90%</i></span>
                <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>

            <div class="progress">
                <span class="skill">Teaching <i class="val">100%</i></span>
                <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>

            <div class="progress">
                <span class="skill">Accounting <i class="val">90%</i></span>
                <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>

            <div class="progress">
                <span class="skill">Being Human<i class="val">100%</i></span>
                <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>

            </div>

        </div>

        </div>
    </section><!-- End Skills Section -->

    <!-- ======= Resume Section ======= -->
    <section id="resume" class="resume">
        <div class="container" data-aos="fade-up">

        <div class="section-title">
            <h2>Resume</h2>
        </div>

        <div class="row">
            <div class="col-lg-6">
            <h3 class="resume-title">Edycation</h3>

            <div class="resume-item">
                <h4>SMA Negeri 1 Singaraja</h4>
            </div>
            <div class="resume-item">
                <h4>SMP Negeri 1 Singarajan</h4>
            </div>
            <div class="resume-item">
                <h4>SD MIN Singaraja</h4>
            </div>
            </div>

            <div class="col-lg-6">
            <h3 class="resume-title">Experience</h3>

            <div class="resume-item">
                <h4>Decoding Beginner Python</h4>
            </div>
            <div class="resume-item">
                <h4>Oracle Java Fundamental</h4>
            </div>
            <div class="resume-item">
                <h4>Finalist National Essai &amp; Gemastik UI/UX</h4>
            </div>
            </div>
        </div>

        </div>
    </section><!-- End Resume Section -->
    
@endsection