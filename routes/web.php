<?php

use App\Http\Controllers\routerController;
use Illuminate\Support\Facades\Route;

Route::get('/', [routerController::class, 'dashboard']) ->name('dashboard');
Route::get('/about', [routerController::class, 'about']) ->name('about');
Route::get('/contact', [routerController::class, 'contact']) ->name('contact');
Route::get('/portofolio', [routerController::class, 'portofolio']) ->name('portofolio');



