<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class routerController extends Controller
{
    public function dashboard()
    {
        return view('dashboard', [
            "title" => "Dashboard"
        ]);
    }

    public function about()
    {
        return view('about', [
            "title" => "About Me"
        ]);
    }

    public function contact()
    {
        return view('contact', [
            "title" => "Contact"
        ]);
    }

    public function portofolio()
    {
        return view('portofolio', [
            "title" => "Portofolio"
        ]);
    }

}
